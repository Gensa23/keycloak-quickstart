/*
 * JBoss, Home of Professional Open Source
 *
 * Copyright 2017 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.keycloak.quickstart.springboot.web;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

/**
 */
@RestController
public class ApplicationController {

    @RequestMapping(value = "/api/{employee}", method = RequestMethod.GET)
    public ResponseEntity<Employee> salary(@PathVariable String employee, Principal principal) {
        String accountId = getAccount(employee);
        if (!hasPerm((KeycloakAuthenticationToken) principal, accountId, "admin"))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        return ResponseEntity.ok(new Employee(employee));
    }

    private String getAccount(String employee) {
        return "10";
    }

    private boolean hasPerm(KeycloakAuthenticationToken principal, String accountId, String role) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        List<String> groupsList = (List<String>) accessToken.getOtherClaims().get("groups");
        List<String> roles = groupsList.stream().filter(group -> group.startsWith(accountId + "_")).map(group -> group.split("_")[1]).collect(Collectors.toList());
        return roles.contains(role);

    }

    public static class Employee {

        private final String name;

        public Employee(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
